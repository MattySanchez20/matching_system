# Matching System

## Task

Looking to enhance the sense of the community in Harman Group, an internal department has suggested an activity where members of different departments meet to exchange experiences while having a cup of coffee. On a monthly basis each person in the pool is randomly matched with someone else in the pool to meet for a 30 min coffee/drink. This could be in person or over Teams. Ideally, the match will be between people from different departments. Staff who want to participate must complete a short form with their details:
- Part of the Harman groupthey work (operations, support, neither of the above)
- Preferred meeting method (In person, online)
- Office where they can access for an in -person meeting (Harman floor 2, Harman floor 3, Rockborne floor 1, No preference)

### Matching Criteria

- They are from different sectors, considering ‘support’ and ‘neither ofthe above’ as the same sector
- Match those with the same preference of meeting method (online or in person)
- They have selected the same officefor an in-person meeting

### Other additional constraints

- They are planning to use an email scheduler to communicate the match
- Basic information for the match to happen is expected as part of the output and additional information regarding the criteria is also expectedas additional attributes
- They are going to send the form to the different departments monthly to enrol new people that would like to join the program. This means that the matches cannot be repeated. The solution should consider to be executed more than once (or to run matches more than once). From the second match round, the people to be matched can be between the people that originally enrolled (match round 1) or those that subsequently joined the program (match round X)
- For every matching iteration, there is a possibility that between groups there will be people without a match (the max numberof possible matches is determine by the group with less members). In this case, the remaining members without a match can be matched within the group
- There should be a record of people without a match

## Results

I made multiple functions that helped me accomplish the task:
- empty_dict: this would created an empty dictionary for each id entered in the function. This was used to make a history dictionary
- dict_combo_v2: this function would create a dictionary with possible combos for every id
- dictionary_adjuster: it is called adjuster because it would remove previous matches (contained in a history dictionary) from an id's possible combos made using dict_combo_v2
- matcher_v3: this would make a pair of ids to match them together. It would ensure no combination is repeated by adding them to the history dictionary
- append_csv: this function would append the match to a csv file for the particular group of people
- redo_round: this would remove the last round's pairs from the respective matching round to be able to re-run the round

To notify the people matched, win32 client was used to access outlook and send the emails.
